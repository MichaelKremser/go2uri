const appId = 'go2uri 0.3.1';
const http = require("http");
const fs = require('fs');
var optionsFileName = '/etc/go2uri/options.js';
var commandLineOptions = { };
process.argv.slice(2).forEach(function (val, index, array) {
    if (val.startsWith('{') && val.endsWith('}')) {
        commandLineOptions = JSON.parse(val)
    }
});

if (commandLineOptions.configfile != undefined) {
    optionsFileName = commandLineOptions.configfile;
}

console.log(`Loading options from file ${optionsFileName}`);
const options = require(optionsFileName);

if (commandLineOptions.port != undefined) {
    options.port = commandLineOptions.port;
}

console.log(`${appId} initializing...`)

const repos = [ ];

if (options.useBuiltInRepo) {
    const builtInRepo = require('./builtInRepo');
    repos.push(builtInRepo);
}

if (options.useRedisRepo) {
    const redisRepo = require('./redisRepo');
    repos.push(redisRepo);
}

if (options.useDbRepo) {
    const dbRepo = require('./dbRepo');
    repos.push(dbRepo);
}

if (options.useXmlRepo) {
    const xmlRepo = require('./xmlRepo');
    repos.push(xmlRepo);
}

if (options.useJsonRepo) {
    const jsonRepo = require('./jsonRepo');
    jsonRepo.init(options.jsonRepoOptions);
    repos.push(jsonRepo);
}

console.log("Repos loaded: " + repos.length);

const started = new Date();
var redirects = 0;
var keyNotFoundCount = 0;

console.log(`${appId} started, listening on port ${options.port}`)

const server = http.createServer(function (req, res) {
    let url = req.url;
  
    if (url === "/") {
        res.writeHead(200, { "Content-Type": "text/html" });
        const startDate = started.toLocaleDateString();
        const startTime = started.toLocaleTimeString('de-de', { hour12: false });
        const redirectsMsg = `${redirects} redirect${(redirects == 1 ? "" : "s")}`;
        const keyNotFoundCountMsg = `${keyNotFoundCount} time${(keyNotFoundCount == 1 ? "" : "s")}`;
        var status_html = fs.readFileSync(options.status_page_file, 'utf-8');
        status_html = status_html
            .replace("{startDate}", startDate)
            .replace("{startTime}", startTime)
            .replace("{redirectsMsg}", redirectsMsg)
            .replace("{keyNotFoundCountMsg}", keyNotFoundCountMsg);
        res.write(status_html);
        res.end();
    } else if (url === "/robots.txt") {
        res.write("User-agent: *\nDisallow: /");
        res.end();
    } else {
        url = url.substring(1);
        const qmPos = url.indexOf('?');
        const key = url.substring(0, qmPos == -1 ? url.length : qmPos );
        const params = qmPos == -1 ? '' : url.substring(qmPos + 1);
        console.log(`key = ${key}; params = ${params}`);
        var foundUri = undefined;
        for (let repoIndex in repos) {
            if (foundUri === undefined || foundUri === null) {
                const repo = repos[repoIndex];
                console.log(`Looking in repo ${repo.toString()} to find an entry for ${req.headers.host} and ${key}`);
                foundUri = repo.getValue(req.headers.host, key);
                console.log('foundUri = ' + foundUri);
            }
            else {
                break; // We found our key/URI, so we are done
            }
        }

        if (typeof foundUri == 'string' && foundUri != null) {
            foundUri = foundUri
                .replace('{params}', params)
                .replace('{url}', url)
                .replace('{host}', req.headers.host);
            console.log(`Redirecting to ${foundUri}`);
            redirects++;
            res.writeHead(302, {
                location: foundUri,
            });
        }
        else {
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.write(`Key ${key} could not be found.`);
            keyNotFoundCount++;
        }
        res.end();
    }
});

server.listen(options.port);