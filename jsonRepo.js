module.exports = {
    init: function(options) {
        this.options = options;
        console.log('I will use ' + options.filename + ' as file name');
    },

    getValue: function(host, key) {
        try {
            this.data = require(this.options.filename);
        } catch (error) {
            console.log("Error when trying to load JSON file: " + error);
            return undefined;
        }
        let hostEntry = this.data[host];
        if (hostEntry === undefined || hostEntry === null) {
            console.log("Looking up in generic host entry");
            hostEntry = this.data["*"];
        }
        else {
            console.log(`Found a host entry for ${host}`);
        }
        let entry = hostEntry[key];
        if (entry === undefined || entry === null) {
            entry = hostEntry["*"];
        }
        return entry;
    },

    toString: function () {
        return 'jsonRepo';
    }
};