// This is an example file! Do not edit.
// To get this working quickly you can execute `ln -s examples/options.js options.js` in the project's root directory.
// If you want to customize according to your needs, copy this file to the project's root directory and edit it accordingly.
module.exports = {
    port: 8080,
    status_page_file: "status.html",
    useBuiltInRepo: true,
    useRedisRepo: true,
    useDbRepo: true,
    useXmlRepo: true,
    useJsonRepo: true,
    jsonRepoOptions: {
        filename: "./data/repo.json"
    }
};