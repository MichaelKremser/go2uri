# go2uri

Looks up a key in key-value stores and redirects to a URI.

## Quick install

    wget https://gitlab.com/MichaelKremser/go2uri/-/raw/main/deploy/install && chmod +x install && ./install

## Creating a LXC container

If you want to run go2uri in a container, you can quickly get one up with this command:

    lxc-create -n go2uri -t debian -- -r bullseye --packages nano,screen,iputils-ping,less,dnsutils,bind9-host,wget,nodejs,mc,git,ca-certificates

After that, use

    lxc-start -d -n go2uri

to start the container. With

    lxc-attach -n go2uri -- bash

you get a bash running in the container and you can execute the commands from "Quick install".

## Quick start

If you want to start the app with the provided example file, use:

    node index.js '{"configfile":"./examples/options.js"}

If you created a config file in `/etc/gouri/options.js`, you can simply use

    node index.js

# Key-value stores

The key-URI-pairs can be saved in various formats and places.

## Internal

The "builtInRepo" repository contains some example URIs.

## Databases

### Redis

To be done

### PostgreSQL

To be done

## File-bassed

### XML

To be done

### JSON

options.js must contain a key like this one:

    jsonRepoOptions: {
        filename: "./data/repo.json"
    }

Example for repo.json:

    {
        "*": {
            "jsonKey1": "http://localhost:8080/",
            "jsonKey2": "http://youtube.com/"
        },
        "foo.example": {
            "bar": "https://bar.foo.example/{params}"
        }
    }

