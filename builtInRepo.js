module.exports = {
    keysAndURIs: {
        test: "http://localhost:8080/",
        search: "https://www.duckduckgo.com/",
        gitlab: "https://gitlab.com/MichaelKremser/"
    },

    getValue: function(host, key) {
        var uri = this.keysAndURIs[key];
        return uri;
    },

    toString: function () {
        return 'builtInRepo';
    }
};